Installation d' Angular CLI

Angular CLI ça veut dire Angular Command Line Interface.

Nous allons l'installer avec npm le gestionnaire de node.js
installez une version spécifique d'angular ou celle par défaut la dernière disponible.

voici les commandes pour procéder à l’installation dans votre IDE
# Installation d'angular-cli dernière version disponible
npm install -g @angular/cli

# Installation d'angular-cli version spécifique
npm install -g @angular/cli@17.0.7

# Test de version installée
ng version

# Générer un projet appelé angular-starter avec choix manuel des options
ng new angular-starter

# Générer un projet appelé angular-starter avec options par défaut
ng new angular-starter --defaults

# Se positionner dans le projet
cd angular-starter

# Exécuter
ng serve

la commande ng serve exécute le projet sur un port par défaut (4200).

Il ne reste qu'à tester le fonctionnement dans un navigateur en lançant l'url suivante.
# Tester
http://localhost:4200
